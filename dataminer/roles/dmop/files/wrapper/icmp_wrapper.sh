#!/bin/bash
# There are 3 predefined variables to make sure the code gets installed with the right version please update them as needed
#sFTPServerKey -> not vital only used to preset the ssh known_host file aginst the sFTP can be done manually 
#DMmd5 & DMOPmd5 -> contain md5sum of code, gets verified everytime you try to install the service
#install also makes sure the following service is installed
#1,11,21,31,41,51 * * * * /bin/bash /opt/dataminer/DMOP/dmop_wrapper.sh 2>&1 >> /opt/dataminer/DMOP/dm-dmop.log
# We are using sshpass to upload the files to sftp with a password. make sure sshpass is in the crontab path (/usr/bin)
#
DATAMINER="/opt/dataminer/bin/dataminer"
DMOP="/opt/dmop/bin/dmop"
DMOP_CONFIG="/opt/dmop/conf/icmp_settings.xml"
LOG_DIR="/var/SevOne/"
WhereShouldCodeRun="ANY" #by defulat should be MASTER or ANY

DATAMINER_CONFIG=`grep config_file $DMOP_CONFIG|grep -v '<!--'|cut -d">" -f2|cut -d"<" -f1`
DATAMINER_OUTPUT_DIR=`grep output_dir $DATAMINER_CONFIG|grep -v '<!--'|cut -d">" -f2|cut -d"<" -f1`
DMOP_OUTPUT_DIR=`grep output_directory $DMOP_CONFIG|grep -v '<!--'|cut -d">" -f2|cut -d"<" -f1`

#Handle installing self wrapper on new local peer machine, should also be called if conf files change and redeployed 
if [  "X$1" == "X--help" ]  ;then 
	echo "Usage: $0 [ --install]"
	echo "If this is a new machine or one that DM or DMOP conf files changed please run:"
	echo "$0 --install"
	echo "once to get changes installed"
	exit 0
fi
if [ $# -eq 1 ] && [ $1 == "--install" ] ;then
	if [ ! -e $DMOP_CONFIG ] || [ ! -e $DATAMINER ] ;then 
		echo "Please copy DM + DMOP files to current machine before trying to install the service"
	fi
	#Make sure output directories exist  cat $DMOP_CONFIG |grep output_directory|sed -s "s/<[/]*output_directory>//g"
	echo "Create required output directories [$DMOP_OUTPUT_DIR $DATAMINER_OUTPUT_DIR ]"
	mkdir -p $DMOP_OUTPUT_DIR
	mkdir -p $DATAMINER_OUTPUT_DIR
	#validate the correct dataminer and dmop code is being deployed
	md5sumRC=0
	DMmd5='3934aa127c59f6e52ee3f2d28fb844d9'
	DMOPmd5='0089ddde3c0c93cfbe07cb3af9884ef6'
	[ `md5sum $DMOP |awk  ' {print $1}'` != $DMOPmd5 ] && echo "failed md5sum chaeck: `md5sum $DMOP` != $DMOPmd5" && md5sumRC=1
	[ `md5sum $DATAMINER |awk  ' {print $1}'` != $DMmd5 ] && echo "failed md5sum chaeck: `md5sum $DATAMINER` != $DMmd5" && md5sumRC=2
	if [ $md5sumRC -ne 0 ]  ;then 
		exit $md5sumRC
	else
		echo "Passed md5sum check of binary code "
	fi


	serverId=`echo "SELECT value FROM local.settings WHERE setting='server_id'"|mysqldata  --skip-column-names`
	peerIp=`echo "select primary_ip from peers where server_id=$serverId"|mysqlconfig --skip-column-names`
	if [ -z $serverId ] || [ -z $peerIp ] ;then 
		echo "Something is worng I can  not determine the server_id or peerIp, install abouurted, please manually fix it and try again"
		exit 2
	fi
	#config DM file to use the local specific Ip as single collector
	echo "Update $DATAMINER_CONFIG to use peer ip=$peerIp"
	sed -i.orig '/<peer ip="/s/=.*/="'$peerIp'" \/>/g' $DATAMINER_CONFIG
	#config DMOP 
	echo "Update $DMOP_CONFIG to use server_id $serverId"
	sed -i.orig '/<peer_id>/s/id>.*/id>'$serverId'<\/peer_id>/g' $DMOP_CONFIG
	#install serviceo
	tmpCron=/tmp/crontab.`date +%s`
	crontab -l > $tmpCron
	if [ `cat $tmpCron |grep dmop_wrapper.sh |wc -l` -eq 0 ] ;then 
		echo "##DM dmop_wrapper.sh installed on `date `" |tee -a $tmpCron
		echo "1,11,21,31,41,51 * * * * /bin/bash $(readlink -f $0)  2>&1 >> $LOG_DIR/dm-dmop.log"|tee -a $tmpCron
		crontab $tmpCron
	else
		echo "Service was alerady installed in the past,skip reinstall,here is current configuration:"
		cat $tmpCron |grep dmop_wrapper.sh
	fi
	#mv old log file aside
	mv  $(dirname $DMOP_CONFIG)/dm-dmop.log  $(dirname $DMOP_CONFIG)/dm-dmop.log.`date +%s` 2>/dev/null
	# create softlink to special sshpass so it can be called from DMOP
	sshPass=$(dirname $DMOP_CONFIG)/sshpass
	if [ -e $sshPass ] ;then 
		ln -s $sshPass /usr/bin/ 2>/dev/null
	else
		echo "I can not find $sshPass DMOP may not be able to upload files to sFTp server" 
	fi
	#This part is none vital DM code will execute fine and in later version we will get ride of the defendency of having the know_hosts even checked 
	#if known_host does not contain the ssh key for our sFTP server add it. you can replace this automation by running once
	#ssh user@sFTp pwd once per peer in the cluster so the required value is created
	sFTPServerKey='172.16.30.78 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAwBrkYWU8KIxE6bWOTi+aKBu8UsYPuW17eqUECIfI0EX1enYRtQwh0JNmN1JXVG9hCDMyYm2emTiR6pu3mmg4hfdOPoIAJIB9oyV4j+1H7e9Hkx4FhAZZXv5s8ZDm0gTYhnPY2k9e9mCdu/oqSwh5ufeCD9T7ZPr0+h0TQGm9kIbynT0g6xEC4SMagmIrkzQGLkrZE7AwH5wtKgylgWBOz6AOd4c8SPeGae+xA+N3IJONbBc/wrDeamqOGemnWPWzpkjV+eOhCnSoSr3s9J0mJTVF6tqqHZXYrFdW0Enr6vnMrRxwxXq8su0Db6vHLHAVFXKg9K3SLxM2/7N/m5u0MQ=='
	sshKnowHostFile=$HOME/.ssh/known_hosts
	if [ `cat $sshKnowHostFile|grep "$sFTPServerKey"|wc -l` -eq 0 ] ;then
		echo "installing sFTP key "
		echo $sFTPServerKey  >> $sshKnowHostFile
	else
		echo "sFTP key already exist nothing to do"
	fi
	exit 0

fi

function log { 
    echo `date +"%Y-%m-%d %H:%M:%S|"` $1
}

function send_mail {
    recipients=`grep "<recipients>" $DMOP_CONFIG|grep -v '<!--'|cut -d">" -f2 |cut -d"<" -f1`
    SevOne-act mail-file --address "$recipients" --mail-sender dataminer@`hostname` \
        --mail-from "SevOne Dataminer" --subject "$1" --body "$2"
}

# Check important files exist
if [ ! -e $DATAMINER ] || [ ! -e $DMOP ] || [ ! -e $DATAMINER_OUTPUT_DIR ] \
    || [ ! -e $DATAMINER_CONFIG ] || [ ! -e $DMOP_CONFIG ] \
    || [ ! -e $DMOP_OUTPUT_DIR ] ; then
    echo "Can't find all the necessary files: "
    ls $DATAMINER $DATAMINER_CONFIG $DMOP $DMOP_CONFIG $DATAMINER_OUTPUT_DIR \
        $DMOP_OUTPUT_DIR > /dev/null
    exit 2
fi

log "############### DATAMINER START ###############"

log "Checking if I'm $WhereShouldCodeRun"
STATUS=$(echo "GET STATUS" | /usr/local/bin.64/masterslaveconsole \
        | grep Message | cut -f3 -d " ")
if  [ X$STATUS != X"$WhereShouldCodeRun" ] || [ X$STATUS != X"ANY" ] ; then
    #run dataminer
    log "I'm $WhereShouldCodeRun - start Dataminer"
    log "Dataminer: process started."
    $DATAMINER $DATAMINER_CONFIG
    dm_return_value=$?
    # check if dataminer finished successfully
    if [ $dm_return_value != "0" ]; then
        log "Dataminer: process failed, return value $dm_return_value."
    else
        log "Dataminer: process finished successfully."
        log "DMOP: process started."
        $DMOP $DMOP_CONFIG
        
        log "FTP: starting to process FTP files." 
        ftp_return_value=$?
        new_file=$(find $DMOP_OUTPUT_DIR -type f -size +0k -mmin -6 -name "*.csv" -printf "%f\n")
        log "FTP: This is latest CSV FILE:  $new_file"
        
        #takes the CSV file replace the delimiter with a tab
        log "FTP: cleaning up these file(s):  $new_file"
        for i in `find $DMOP_OUTPUT_DIR -type f -size +0k -mmin -5 -name "*.csv" -printf "%f\n"`; do  sed -i -e 's/\^/\t/g' -i -e 's/\r//g' $DMOP_OUTPUT_DIR/$i; done
  
	#ftp the cvs file
	log "FTP: Sending FTP files to FTP Server."
	for i in `find $DMOP_OUTPUT_DIR -type f -size +0k -mmin -6 -name "*.csv" -printf "%f\n"`; do /bin/sh /opt/dmop/ftp/ftp_icmp.sh $i; done

        dmop_return_value=$?
        if [ $dmop_return_value == "0" ]; then
            log "DMOP: process finished successfully."
        else
            log "DMOP: process failed, return value $dmop_return_value."
            emailing=$(grep -Pzo "(?s)<emailing>.*?</emailing>" $DMOP_CONFIG \
                |grep -v '<!--'|grep enabled|cut -d">" -f2|cut -d"<" -f1)
            if [ $emailing == "true" ]; then
                now=`date +"%Y-%m-%d %H:%M"`
                peer=`hostname`
                config_file=$(<$DMOP_CONFIG)
                subject="SevOne Data Export Failure on Peer $peer"
                message="Data Export has failed on SevOne peer $peer on $now.
DM was attempting to run using the following config file:
$config_file"
                send_mail "${subject}" "${message}"
            fi
        fi
    fi
    # clean output files older than 2 hours
    log "Clean files older than 2 hours"
    find $DATAMINER_OUTPUT_DIR  -cmin +120 -exec rm {} \;
    find $DMOP_OUTPUT_DIR  -cmin +120 -exec rm {} \;
else
    log "I'm not $WhereShouldCodeRun - exit"
    log "################ DATAMINER END ################"
    exit 4
fi
log "################ DATAMINER END ################"
